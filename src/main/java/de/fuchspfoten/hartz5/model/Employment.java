package de.fuchspfoten.hartz5.model;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.hartz5.Formulas;
import de.fuchspfoten.hartz5.Hartz5Plugin;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.UUID;

/**
 * Represents an employment of a player.
 */
public class Employment {

    /**
     * The job that this employment is in.
     */
    private @Getter final Job job;

    /**
     * The amount of XP that the player currently has.
     */
    private @Getter long xp;

    /**
     * The total amount of XP that the player currently has.
     */
    private @Getter long xpTotal;

    /**
     * The level that the player currently has.
     */
    private @Getter int level;

    /**
     * The accumulated money.
     */
    private double moneyAccumulated;

    /**
     * Constructor.
     *
     * @param job The job.
     * @param xp The current xp.
     * @param xpTotal The total xp.
     * @param level The job level.
     */
    public Employment(final Job job, final long xp, final long xpTotal, final int level) {
        this.job = job;
        this.xp = xp;
        this.xpTotal = xpTotal;
        this.level = level;
    }

    /**
     * Returns the amount of XP needed for the next level.
     *
     * @return The amount of XP needed.
     */
    public long getXpForNextLevel() {
        return Formulas.getXpForLevelUp(level);
    }

    /**
     * Pays the accumulated money.
     *
     * @param player The target player who receives the payment.
     */
    public void payout(final UUID player) {
        if (moneyAccumulated > 1) {
            Hartz5Plugin.getSelf().getEconomy().depositPlayer(Bukkit.getOfflinePlayer(player), moneyAccumulated);
            moneyAccumulated = 0;
        } else if (moneyAccumulated < -1) {
            Hartz5Plugin.getSelf().getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(player), -moneyAccumulated);
            moneyAccumulated = 0;
        }
    }

    /**
     * Performs the given entry.
     *
     * @param target The target for information messages.
     * @param entry The entry.
     * @param multiplier The multiplier for all actions.
     */
    public void performEntry(final CommandSender target, final Entry entry, final int multiplier) {
        final double levelMulti = Math.min(Formulas.getLevelMultiplier(level),
                Hartz5Plugin.getSelf().getMaxMultiplier());
        final double globalMulti = levelMulti * multiplier;
        moneyAccumulated += globalMulti * entry.getMoney();
        awardXP(target, entry.getXp() * multiplier);
    }

    /**
     * Awards the given number of XP. Might be negative.
     *
     * @param target The target for information messages.
     * @param amount The amount of XP.
     */
    private void awardXP(final CommandSender target, final long amount) {
        if (amount < 0) {
            // Remove XP.
            final long correctedAmount = -amount;
            xpTotal -= correctedAmount;
            if (amount <= xp) {
                // Easy: Just subtract.
                xp -= correctedAmount;
            } else if (level > 0) {
                // Level-Down.
                final long amountLeft = correctedAmount - xp;
                xp = Formulas.getXpForLevelUp(level - 1) - amountLeft;
                level--;
                Messenger.send(target, "hartz5.levelDown", job.getName(), level);
            } else {
                // Level 0: Can't reduce level. Just set to 0.
                xp = 0;
            }
        } else {
            // Add XP.
            xpTotal += amount;
            xp += amount;
            if (xp >= getXpForNextLevel()) {
                // Level-Up.
                xp -= getXpForNextLevel();
                level++;
                Messenger.send(target, "hartz5.levelUp", job.getName(), level);
            } else {
                // Easy: Just add.
                // (do nothing)
            }
        }
    }
}
