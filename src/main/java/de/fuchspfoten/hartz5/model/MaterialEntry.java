package de.fuchspfoten.hartz5.model;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * An entry for a material.
 */
public class MaterialEntry extends Entry {

    /**
     * The material for this entry.
     */
    private @Getter final Material material;

    /**
     * The item stack for display.
     */
    private final ItemStack displayStack;

    /**
     * Constructor.
     *
     * @param xp The xp for this entry.
     * @param money The money for this entry.
     * @param name The name of the material.
     */
    public MaterialEntry(final long xp, final double money, final String name) {
        super(xp, money);

        // Load the material.
        material = Material.getMaterial(name);
        if (material == null) {
            throw new IllegalArgumentException("Invalid material!");
        }

        // Create the display item.
        displayStack = new ItemStack(material, 1);
    }

    @Override
    public ItemStack getDisplayItem() {
        return displayStack;
    }
}
