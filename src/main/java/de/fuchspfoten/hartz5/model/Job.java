package de.fuchspfoten.hartz5.model;

import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a job.
 */
public class Job {

    /**
     * The ID of the job.
     */
    private @Getter final String id;

    /**
     * The name of this job.
     */
    private @Getter final String name;

    /**
     * The rewards for this job.
     */
    private final Map<ActionType, Map<String, Entry>> rewardMap = new EnumMap<>(ActionType.class);

    /**
     * Constructor.
     *
     * @param id The ID of the job.
     * @param jobSection The configuration section for the job.
     */
    public Job(final String id, final ConfigurationSection jobSection) {
        this.id = id;
        name = jobSection.getString("name");

        // Load the reward map.
        for (final String key : jobSection.getKeys(false)) {
            // Determine the action type.
            final ActionType type = ActionType.getForKey(key);
            if (type == null) {
                continue;
            }
            rewardMap.put(type, new HashMap<>());

            // Load all entries.
            final ConfigurationSection actionSection = jobSection.getConfigurationSection(key);
            for (final String entryName : actionSection.getKeys(false)) {
                final ConfigurationSection entrySection = actionSection.getConfigurationSection(entryName);
                final Entry entry = Entry.createEntryFor(type, entryName, entrySection);
                if (entry == null) {
                    continue;
                }

                rewardMap.get(type).put(entryName, entry);
            }
        }
    }

    /**
     * Fetches the entry for the given action type and key. The job must have entries for the given action type.
     *
     * @param actionType The action type.
     * @param key The key.
     * @return The entry or null if it does not exist.
     */
    public Entry getEntry(final ActionType actionType, final String key) {
        return rewardMap.get(actionType).get(key);
    }

    /**
     * Checks whether this job has entries for the given type.
     *
     * @param actionType The action type.
     * @return Whether there are entries for the given action type.
     */
    public boolean hasEntriesFor(final ActionType actionType) {
        return rewardMap.containsKey(actionType);
    }
}
