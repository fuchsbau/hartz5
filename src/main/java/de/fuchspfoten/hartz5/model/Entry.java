package de.fuchspfoten.hartz5.model;

import de.fuchspfoten.hartz5.Hartz5Plugin;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

/**
 * Represents an entry in the job configuration.
 */
@RequiredArgsConstructor
public abstract class Entry {

    /**
     * Creates an entry with the given action type for the given section.
     *
     * @param type The entry type.
     * @param entryName The entry name.
     * @param entrySection The given entry section.
     * @return The entry.
     */
    public static Entry createEntryFor(final ActionType type, final String entryName,
                                       final ConfigurationSection entrySection) {
        final long xp = entrySection.getLong("xp");
        final double money = entrySection.getDouble("money");

        switch (type) {
            case BREAK:
            case PLACE:
                try {
                    return new MaterialEntry(xp, money, entryName);
                } catch (final IllegalArgumentException ex) {
                    Hartz5Plugin.getSelf().getLogger().severe("Error in job entry!");
                    ex.printStackTrace();
                    return null;
                }
        }
        return null;
    }

    /**
     * The awarded XP.
     */
    protected @Getter final long xp;

    /**
     * The awarded money.
     */
    protected @Getter final double money;

    /**
     * Returns an item for displaying this entry. May be the same item at every call.
     * <p>
     * The caller must not modify the returned value.
     *
     * @return The item.
     */
    public abstract ItemStack getDisplayItem();
}
