package de.fuchspfoten.hartz5.model;

/**
 * The available action types.
 */
public enum ActionType {

    /**
     * Breaking blocks.
     */
    BREAK,

    /**
     * Placing blocks.
     */
    PLACE;

    /**
     * Returns the correct ActionType for the given configuration key.
     *
     * @param key The configuration key.
     * @return The action type.
     */
    public static ActionType getForKey(final String key) {
        switch (key) {
            case "onBreak": return BREAK;
            case "onPlace": return PLACE;
            default: return null;
        }
    }
}
