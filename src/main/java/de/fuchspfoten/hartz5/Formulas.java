package de.fuchspfoten.hartz5;

/**
 * Formula utility class.
 */
public final class Formulas {

    /**
     * Returns the XP needed for levelling up on the given level.
     *
     * @param level The level.
     * @return The XP needed.
     */
    public static long getXpForLevelUp(final int level) {
        return (long) level * level * level + 1500;
    }

    /**
     * Returns the multiplier for the given level.
     * @param level The level.
     * @return The multiplier.
     */
    public static double getLevelMultiplier(final int level) {
        return 1.0 + 0.04 * level;
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Formulas() {
    }
}
