package de.fuchspfoten.hartz5.modules;

import de.fuchspfoten.hartz5.Hartz5Plugin;
import de.fuchspfoten.hartz5.model.ActionType;
import de.fuchspfoten.hartz5.model.Employment;
import de.fuchspfoten.hartz5.model.Entry;
import de.fuchspfoten.hartz5.model.Job;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * A module that cares for paying out money and xp.
 */
public class PayoutModule implements Listener {

    /**
     * The locations where blocks have been placed. These do not award benefits when breaking.
     */
    private final Collection<Location> taintedLocations = new HashSet<>();

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockBreak(final BlockBreakEvent event) {
        if (event.isCancelled()) {
            return;
        }

        // Check disabled worlds.
        if (Hartz5Plugin.getSelf().getDisabledWorldsBreakPlace().contains(event.getBlock().getWorld().getName())) {
            return;
        }

        // Check the location if there was a block placed here.
        if (taintedLocations.contains(event.getBlock().getLocation())) {
            return;
        }

        // Check for game mode.
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }

        // Pay out employees.
        final Set<Employment> employments = Hartz5Plugin.getSelf().getEmployments(event.getPlayer());
        for (final Employment emp : employments) {
            final Job job = emp.getJob();

            // Break payout.
            if (job.hasEntriesFor(ActionType.BREAK)) {
                final Entry applicable = job.getEntry(ActionType.BREAK, event.getBlock().getType().name());
                if (applicable == null) {
                    continue;
                }
                emp.performEntry(event.getPlayer(), applicable, 1);
            }

            // Place penalty.
            if (job.hasEntriesFor(ActionType.PLACE)) {
                final Entry applicable = job.getEntry(ActionType.PLACE, event.getBlock().getType().name());
                if (applicable == null) {
                    continue;
                }
                emp.performEntry(event.getPlayer(), applicable, -1);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlace(final BlockPlaceEvent event) {
        if (event.isCancelled()) {
            return;
        }

        // Check disabled worlds.
        if (Hartz5Plugin.getSelf().getDisabledWorldsBreakPlace().contains(event.getBlock().getWorld().getName())) {
            return;
        }

        // Taint the location.
        taintedLocations.add(event.getBlockPlaced().getLocation());

        // Check for game mode.
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }

        // Pay out employees.
        final Set<Employment> employments = Hartz5Plugin.getSelf().getEmployments(event.getPlayer());
        for (final Employment emp : employments) {
            final Job job = emp.getJob();

            // Place payout.
            if (job.hasEntriesFor(ActionType.PLACE)) {
                final Entry applicable = job.getEntry(ActionType.PLACE, event.getBlock().getType().name());
                if (applicable == null) {
                    continue;
                }
                emp.performEntry(event.getPlayer(), applicable, 1);
            }

            // Break penalty.
            if (job.hasEntriesFor(ActionType.BREAK)) {
                final Entry applicable = job.getEntry(ActionType.BREAK, event.getBlock().getType().name());
                if (applicable == null) {
                    continue;
                }
                emp.performEntry(event.getPlayer(), applicable, -1);
            }
        }
    }
}
