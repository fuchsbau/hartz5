package de.fuchspfoten.hartz5.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.hartz5.Hartz5Plugin;
import de.fuchspfoten.hartz5.model.Employment;
import de.fuchspfoten.hartz5.model.Job;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Set;

/**
 * /jobs is used for controlling the jobs system.
 */
public class JobsControlModule implements CommandExecutor {

    /**
     * Argument parser for /jobs.
     */
    private final ArgumentParser argumentParserJobs;

    /**
     * Constructor.
     */
    public JobsControlModule() {
        Messenger.register("hartz5.jobNotExisting");
        Messenger.register("hartz5.jobsHelp.stats");
        Messenger.register("hartz5.jobsHelp.join");
        Messenger.register("hartz5.jobsHelp.leave");
        Messenger.register("hartz5.join.exceed");
        Messenger.register("hartz5.join.exceedOther");
        Messenger.register("hartz5.join.already");
        Messenger.register("hartz5.join.alreadyOther");
        Messenger.register("hartz5.join.done");
        Messenger.register("hartz5.join.doneOther");
        Messenger.register("hartz5.leave.notHaving");
        Messenger.register("hartz5.leave.notHavingOther");
        Messenger.register("hartz5.leave.done");
        Messenger.register("hartz5.leave.doneOther");
        Messenger.register("hartz5.stats.initial");
        Messenger.register("hartz5.stats.initialOther");
        Messenger.register("hartz5.stats.line");

        argumentParserJobs = new ArgumentParser();
        argumentParserJobs.addArgument('u', "Performs the operation as the given user",
                "user");
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("hartz5.use")) {
            sender.sendMessage("Missing permission: hartz5.use");
            return true;
        }

        // Syntax check.
        args = argumentParserJobs.parse(args);
        if (argumentParserJobs.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /jobs");
            argumentParserJobs.showHelp(sender);
            return true;
        }

        // Display help for users.
        if (args.length == 0) {
            Messenger.send(sender, "hartz5.jobsHelp.stats");
            Messenger.send(sender, "hartz5.jobsHelp.join");
            Messenger.send(sender, "hartz5.jobsHelp.leave");
            return true;
        }

        // Fetch the executor.
        final OfflinePlayer target;
        if (argumentParserJobs.hasArgument('u') && sender.hasPermission("hartz5.sudo")) {
            target = Bukkit.getOfflinePlayer(UUIDLookup.lookup(argumentParserJobs.getArgument('u')));
            if (target == null) {
                sender.sendMessage("target not found");
                return true;
            }
        } else {
            if (!(sender instanceof OfflinePlayer)) {
                sender.sendMessage("players only!");
                return true;
            }
            target = (OfflinePlayer) sender;
        }

        // Fetch the subcommand and execute it.
        final String subCommand = args[0];
        if (subCommand.equalsIgnoreCase("stats")) {
            jobsStats(sender, target);
        } else if (subCommand.equalsIgnoreCase("join") && args.length == 2) {
            jobsJoin(sender, target, args[1]);
        } else if (subCommand.equalsIgnoreCase("leave") && args.length == 2) {
            jobsLeave(sender, target, args[1]);
        }

        return true;
    }

    /**
     * Leaves a job.
     *
     * @param sender The sender who requested the change.
     * @param target The target to whom the change is applied.
     * @param jobName The job name.
     */
    private static void jobsLeave(final CommandSender sender, final OfflinePlayer target, final String jobName) {
        // Fetch the employments.
        final Set<Employment> employments = Hartz5Plugin.getSelf().getEmployments(target);

        // Fetch the job.
        Job job = null;
        for (final Job candidate : Hartz5Plugin.getSelf().getJobMap().values()) {
            if (candidate.getName().equals(jobName)) {
                job = candidate;
                break;
            }
        }
        if (job == null) {
            Messenger.send(sender, "hartz5.jobNotExisting");
            return;
        }

        // Check that the job is taken.
        Employment emp = null;
        for (final Employment empHaving : employments) {
            if (empHaving.getJob() == job) {
                emp = empHaving;
                break;
            }
        }
        if (emp == null) {
            if (target == sender) {
                Messenger.send(sender, "hartz5.leave.notHaving");
            } else {
                Messenger.send(sender, "hartz5.leave.notHavingOther", target.getName());
            }
            return;
        }

        // Unemploy the player.
        Hartz5Plugin.getSelf().unemploy(target, job);
        if (target == sender) {
            Messenger.send(sender, "hartz5.leave.done");
        } else {
            Messenger.send(sender, "hartz5.leave.doneOther", target.getName());
        }
    }

    /**
     * Joins a job.
     *
     * @param sender The sender who requested the change.
     * @param target The target to whom the change is applied.
     * @param jobName The job name.
     */
    private static void jobsJoin(final CommandSender sender, final OfflinePlayer target, final String jobName) {
        // Fetch the employments and test for the limit.
        final Set<Employment> employments = Hartz5Plugin.getSelf().getEmployments(target);
        if (employments.size() == Hartz5Plugin.getSelf().getMaxJobs()) {
            if (target == sender) {
                Messenger.send(sender, "hartz5.join.exceed");
            } else {
                Messenger.send(sender, "hartz5.join.exceedOther", target.getName());
            }
            return;
        }

        // Fetch the job.
        Job job = null;
        for (final Job candidate : Hartz5Plugin.getSelf().getJobMap().values()) {
            if (candidate.getName().equals(jobName)) {
                job = candidate;
                break;
            }
        }
        if (job == null) {
            Messenger.send(sender, "hartz5.jobNotExisting");
            return;
        }

        // Check that the job is not already taken.
        for (final Employment empHaving : employments) {
            if (empHaving.getJob() == job) {
                if (target == sender) {
                    Messenger.send(sender, "hartz5.join.already");
                } else {
                    Messenger.send(sender, "hartz5.join.alreadyOther", target.getName());
                }
                return;
            }
        }

        // Create the employment.
        final Employment emp = new Employment(job, 0, 0, 0);
        Hartz5Plugin.getSelf().employ(target, emp);
        if (target == sender) {
            Messenger.send(sender, "hartz5.join.done");
        } else {
            Messenger.send(sender, "hartz5.join.doneOther", target.getName());
        }
    }

    /**
     * Displays job stats.
     *
     * @param sender The sender who requested the change.
     * @param target The target to whom the change is applied.
     */
    private static void jobsStats(final CommandSender sender, final OfflinePlayer target) {
        if (target == sender) {
            Messenger.send(sender, "hartz5.stats.initial");
        } else {
            Messenger.send(sender, "hartz5.stats.initialOther", target.getName());
        }

        // Fetch the employments and display stats.
        final Set<Employment> employments = Hartz5Plugin.getSelf().getEmployments(target);
        for (final Employment emp : employments) {
            Messenger.send(sender, "hartz5.stats.line", emp.getJob().getName(), emp.getLevel(), emp.getXp(),
                    emp.getXpForNextLevel());
        }
    }
}
