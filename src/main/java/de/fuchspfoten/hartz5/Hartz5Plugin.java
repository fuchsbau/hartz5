package de.fuchspfoten.hartz5;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.hartz5.model.Employment;
import de.fuchspfoten.hartz5.model.Job;
import de.fuchspfoten.hartz5.modules.JobsControlModule;
import de.fuchspfoten.hartz5.modules.PayoutModule;
import lombok.Getter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

/**
 * The main plugin class.
 */
public class Hartz5Plugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static Hartz5Plugin self;

    /**
     * A map that maps a job's ID to its data structure.
     */
    private @Getter final Map<String, Job> jobMap = new HashMap<>();

    /**
     * Maps UUIDs to a set of jobs that are held.
     */
    private final Map<UUID, Set<Employment>> jobCache = new HashMap<>();

    /**
     * A set of names of worlds in which jobs are disabled.
     */
    private @Getter final Set<String> disabledWorlds = new HashSet<>();

    /**
     * A set of names of worlds in which jobs (w.r.t. break and place) are disabled.
     */
    private @Getter final Set<String> disabledWorldsBreakPlace = new HashSet<>();

    /**
     * The economy API.
     */
    private @Getter Economy economy;

    /**
     * The maximum number of jobs a player can have.
     */
    private @Getter int maxJobs;

    /**
     * The maximum multiplier that a level can have on xp and money.
     */
    private @Getter double maxMultiplier;

    @Override
    public void onEnable() {
        self = this;

        // Register messages.
        Messenger.register("hartz5.levelUp");
        Messenger.register("hartz5.levelDown");

        // Load the economy.
        economy = getServer().getServicesManager().load(Economy.class);
        if (economy == null) {
            throw new IllegalStateException("economy not present during enable!");
        }

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Load all jobs.
        final ConfigurationSection jobSection = getConfig().getConfigurationSection("jobs");
        for (final String jobId : jobSection.getKeys(false)) {
            jobMap.put(jobId, new Job(jobId, jobSection.getConfigurationSection(jobId)));
            getLogger().info("Job registered: " + jobId);
        }

        // Load job limit and maximum multiplier.
        maxJobs = getConfig().getInt("maxJobs");
        maxMultiplier = getConfig().getDouble("maxMultiplier");

        // Load disabled worlds.
        disabledWorlds.addAll(getConfig().getStringList("disabledWorlds"));
        disabledWorldsBreakPlace.addAll(getConfig().getStringList("disabledBP"));

        // Register command executor modules.
        getCommand("jobs").setExecutor(new JobsControlModule());

        // Register modules.
        getServer().getPluginManager().registerEvents(new PayoutModule(), this);

        // Register sync task.
        getServer().getScheduler().scheduleSyncRepeatingTask(this, this::syncCache, 180 * 20L,
                180 * 20L);

        // Register payout task.
        getServer().getScheduler().scheduleSyncRepeatingTask(this, this::payoutCache, 30 * 20L,
                30 * 20L);
    }

    @Override
    public void onDisable() {
        getLogger().info("Syncing cache!");
        syncCache();
    }

    /**
     * Fetch the employments for the given player.
     *
     * @param player The player.
     * @return The employments of this player.
     */
    public Set<Employment> getEmployments(final OfflinePlayer player) {
        // Check cache entry.
        if (!jobCache.containsKey(player.getUniqueId())) {
            // Load cache entry.
            final PlayerData pd = new PlayerData(player);
            final ConfigurationSection employmentSection;
            if (pd.getStorage().isConfigurationSection("jobs.employments")) {
                employmentSection = pd.getStorage().getConfigurationSection("jobs.employments");
            } else {
                employmentSection = pd.getStorage().createSection("jobs.employments");
            }

            // Create an empty employment set.
            jobCache.put(player.getUniqueId(), new HashSet<>());

            // Retrieve the employments.
            for (final String jobKey : employmentSection.getKeys(false)) {
                final Job job = jobMap.get(jobKey);
                if (job == null) {
                    continue;
                }
                final ConfigurationSection jobSection = employmentSection.getConfigurationSection(jobKey);
                final Employment employment = new Employment(job, jobSection.getLong("xp"),
                        jobSection.getLong("xpTotal"), jobSection.getInt("level"));
                jobCache.get(player.getUniqueId()).add(employment);
            }
        }

        // Return the cached employment set.
        return jobCache.get(player.getUniqueId());
    }

    /**
     * Employs a player in the given job employment.
     *
     * @param player The player.
     * @param emp The job employment.
     */
    public void employ(final OfflinePlayer player, final Employment emp) {
        // Cache the change.
        getEmployments(player).add(emp);

        // Store the change.
        final PlayerData pd = new PlayerData(player);
        pd.getStorage().set("jobs.employments." + emp.getJob().getId() + ".xp", 0);
        pd.getStorage().set("jobs.employments." + emp.getJob().getId() + ".xpTotal", 0);
        pd.getStorage().set("jobs.employments." + emp.getJob().getId() + ".level", 0);
    }

    /**
     * Unemploys a player in the given job.
     *
     * @param player The player.
     * @param job The job.
     */
    public void unemploy(final OfflinePlayer player, final Job job) {
        // Cache the change.
        Employment old = null;
        for (final Employment other : getEmployments(player)) {
            if (other.getJob() == job) {
                old = other;
                break;
            }
        }
        if (old == null) {
            return;
        }
        getEmployments(player).remove(old);

        // Store the change.
        final PlayerData pd = new PlayerData(player);
        pd.getStorage().set("jobs.employments." + job.getId(), null);
    }

    /**
     * Syncs the employment cache.
     */
    private void syncCache() {
        final Iterable<UUID> cachedIds = new HashSet<>(jobCache.keySet());
        for (final UUID player : cachedIds) {
            final PlayerData pd = new PlayerData(player);
            for (final Employment emp : jobCache.get(player)) {
                pd.getStorage().set("jobs.employments." + emp.getJob().getId() + ".xp", emp.getXp());
                pd.getStorage().set("jobs.employments." + emp.getJob().getId() + ".xpTotal", emp.getXpTotal());
                pd.getStorage().set("jobs.employments." + emp.getJob().getId() + ".level", emp.getLevel());
            }

            // Remove offline players from the cache.
            final Player onlinePlayer = Bukkit.getPlayer(player);
            if (onlinePlayer == null || !onlinePlayer.isOnline()) {
                jobCache.remove(player);
            }
        }
    }

    /**
     * Pays all outstanding balances.
     */
    private void payoutCache() {
        for (final Entry<UUID, Set<Employment>> jobEntry : jobCache.entrySet()) {
            for (final Employment emp : jobEntry.getValue()) {
                emp.payout(jobEntry.getKey());
            }
        }
    }
}
